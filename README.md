Assignment 4 - Using React - Fetch API and Promises

Cathy Tham

Goal of this project is to create an Weather App by using React and asynchronous JavaScript programming.The weather data was provided by https://openweathermap.org/current. When the user input a city and click on the "Get Weather" button, the application will make a GET request to the Open weather api and display the current weather and an image depending on the input. 
If the user enter a valid city, the response is 200 OK and a JSON string will be return in response.  
If the city name is not found, it will return an 404 error and an city not found message will be display on the page.  

