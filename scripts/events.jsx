// Cathy Tham 1944919 

/**
 * This program will use the open weather map to display the description, the temperature and an image of the current weather
 * of a corresponding city enter by the user.
 * It gets the weather information by fetching from the API (https://api.openweathermap.org/data/2.5/) 
 * @author Cathy Tham
 */
"use strict";

/**
 * This function renders the form.
 */
function setup() {
    ReactDOM.render(<WeatherForm title="Weather App"/>, document.querySelector('#form'))
}

/**
 * This class is called when ReactDOM is render.
 * It contains methods that help to create the form, fecth and display the weather to the user.
 */
class WeatherForm extends React.Component {
    /**
     * This constructor is used to declare global variables and some states properties.   
     * It calls the event handler for form submit and also invoke the callback passed in props.
     * @param {*} props Properties that are immutable
     */
    constructor(props) {
        super(props);
        this.state = {
            showError: false, 
            showResult: false,
            errorMsg:""
        };
        this.apiURL="https://api.openweathermap.org/data/2.5/";
        this.APIKEY="285c52998154358af2fde2489d994c85"; 
        this.iconURLBase="http://openweathermap.org/img/wn/";
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
      
    /**
     * This method update React component state when there is input in the form.
     * @param {event} e 
     */
    handleChange(e) {
        console.log("in change(): "+e.target.value)
        const value = e.target.value;
        this.setState({
            city: value
        });
      }

    /**
     * This method call the fetchAPI mehtod and reset the input whent he form is submitted.
     * @param {event} e 
    */
    handleSubmit(e){
        e.preventDefault();
        console.log('fetch data ' + this.state.city);
        this.getURL();
        //this.fetchAPI(fullURL);
        e.target.reset();
    }

    /**
     * This method uses the base url and add the user input, "metric"(to retrieve temp in celcius) and
     * API key to the corresponding elements in the URL.
     * It calls the fetchAPI() 
     */
    getURL(){
        const data = this.state.city;
        let url = new URL(this.apiURL+"weather");
        url.searchParams.set("q", data);
        url.searchParams.set("units","metric");
        url.searchParams.set("appid", this.APIKEY);
        console.log(url);
        this.fetchAPI(url);
    }
    /**
     * This method is used to get the weather information based on the city name entered by using fetch and promise.
     * If the response if 200 (ok), it will call the displayWeather().
     * Else, it will throw a error.
     * @param {URL} url 
     */
    fetchAPI(url){ 

        fetch(url)
            .then(response => {
                if (response.ok) {
                    return response.json();
                }
                 else if(response.status === 404) {
                    throw new Error(`Error! City not found`);
                }
                 else {
                    throw new Error(`HTTP error! status: ${response.status}`);
                }
            }).then(json => this.displayWeather(json))
            .catch(error => this.handleError(error, url));
    }

    /**
     * This method is used to set the state of showError to true and showResult to false so that it only display the error message.
     * It also put the error message into the state errorMsg.
     * @param {error} error Error 
     * @param {String} url 
     */
    handleError(error, url) {
        console.log("Error "+error.message +  " ");
        console.log("url : " + url);
        this.setState({ showError: true, showResult: false, errorMsg: error.message});
    }

    /**
     * This function is used to set the state to display the weather to the user when the fetch is successful 
     * and calls the displayImage to display the corresponding image.
     * @param {JSON} json 
     */
    displayWeather(json){
        console.log(json);
        this.setState({ showError: false, showResult: true });
        this.setState({ temp: Math.round(json.main.temp), 
                        description: json.weather[0].description, 
                        icon: json.weather[0].icon} );
        this.displayImage();
    }

    /**
     * This method is used to get the url to display the image and add it to the state.
     */
    displayImage(){
        const data= this.state.icon;
        const url=new URL (this.iconURLBase+data+"@2x.png");
        console.log(url);
        this.setState({ iconURL: url} );
    }

    /**
     * This renders method is used to create the elements for the index and depending on the state os showResult and showError,
     * it will display the weather information or the error text.
     */
    render() {
        const styleResult = { 
            display: this.state.showResult ? "block" : "none"}
        const styleError = { 
               display: this.state.showError ? "block" : "none"}
        return (
            <article>
                <h1>{this.props.title}</h1>
                <form onSubmit={this.handleSubmit}>
                    <label id='label' htmlFor='city'>City: </label>
                    <input id="city" name="city" type="text" placeholder="Enter a city" onChange={this.handleChange} required />
                    <button id="submitButton" type="submit">Get Weather</button>
                </form>

                <div id='result' style={ styleResult }>
                    <p id="weather">Description: {this.state.description} <br></br> Temp:{this.state.temp} &deg;C</p>
                    <img id="image" src={this.state.iconURL}></img>
                </div>
                <div id='error' style={styleError}>
                    <p id="error">{this.state.errorMsg}</p>
                </div>

            </article>
        );
        }
}
setup();